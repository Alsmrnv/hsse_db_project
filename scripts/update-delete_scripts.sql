with deleted_rows as (
    delete
    from university_database.student_list
    where faculty = 'ФПМИ' and status = 'Отчислен'
    returning *
)
update university_database.faculties
set num_of_free_budget_funded_seats = num_of_free_budget_funded_seats + (
    select count(*)
    from deleted_rows
    where fee_status = 'Бюджет'
    ),
    num_of_free_non_budget_seats = faculties.num_of_non_budget_seats + (
    select count(*)
    from deleted_rows
    where fee_status in ('Платная основа', 'Грант')
    )
where faculty = 'ФПМИ';

delete
from university_database.student_list
where faculty = 'ФБМФ' and group_name = 'Б95-001';

update university_database.group
set group_gpa = 0, number_of_students = 0
where group_name = 'Б95-001';


delete
from university_database.student_list
where birth_date < '2002-01-01' and status = 'В академе';