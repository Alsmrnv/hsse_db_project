select faculty, max(gpa) as max_gpa
from university_database.student_list
group by faculty;

select group_name, max(gpa) as max_gpa
from university_database.student_list
where faculty = 'ВШПИ'
group by group_name;

select group_name, count(*) as cnt
from university_database.student_list
group by group_name
having count(*) <=3
order by cnt;

select faculty, max(num_of_free_budget_funded_seats) as max_free_seats
from university_database.faculties
group by faculty;

select count(*) as cnt
from university_database.group
where group_gpa > 8.0;

---------------------------------

select name, surname, group_name, round(gpa * 100 / sum(gpa) over w) as gpa_perc
from university_database.student_list
window w as (
    partition by group_name
)
order by gpa_perc;

select dense_rank() over w as rank, name, surname, group_name, gpa
from university_database.student_list
window w as (
    partition by group_name
    order by gpa desc
);

select name, surname, group_name, gpa, round(gpa * 100 / lag(gpa, 1) over w) as gpa_to_prev_gpa_perc
from university_database.student_list
window w as (
    partition by group_name
    order by gpa
);

select ntile(5) over w as tile, group_name, group_gpa
from university_database.group
window w as (
    order by group_gpa desc
);

select name, dense_rank() over w as rank, surname, group_name, fee_status, gpa
from university_database.student_list
window w as (
  partition by fee_status
  order by gpa
);