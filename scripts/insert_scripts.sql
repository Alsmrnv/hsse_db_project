-- ДИСКЛЕЙМЕР
-- Все персонажи, термины и описываемые собития - вымышлены.
-- Любое совпадение с реальными людьми и событиями - случайно

insert into university_database.faculties (faculty)
values ('ФПМИ'), ('ФРКТ'), ('ЛФИ'), ('ФАКТ'), ('ВШПИ'), ('ФБМФ');

insert into university_database.group (group_name)
values ('Б99-001'), ('Б99-002'), ('Б99-003'), ('Б98-001'), ('Б98-002'), ('Б98-003'), ('Б97-001'), ('Б97-002'),
       ('Б97-003'), ('Б96-001'), ('Б96-002'), ('Б96-003'), ('Б01-001'), ('Б01-002'), ('Б01-003'), ('Б95-001'),
       ('Б95-002'), ('Б95-003');

insert into university_database.fee_info (fee_status)
values ('Бюджет'), ('Платная основа'), ('Грант');

insert into university_database.student_list (name, surname, patronymic, birth_date, faculty, group_name, gpa, fee_status, status)
values ('Алиса', 'Семенова', 'Егоровна', '2002-01-13', 'ФПМИ', 'Б99-001', 6.7, 'Бюджет', 'Учится'),
       ('Иван', 'Ткачев', 'Михайлович', '2003-02-11', 'ФПМИ', 'Б99-001', 7.6, 'Бюджет', 'Учится'),
       ('Никита', 'Захаров', 'Абрамович', '2005-09-22', 'ФПМИ', 'Б99-001', 5.2, 'Бюджет', 'Учится'),
       ('Виктория', 'Громова', 'Семеновна', '2006-06-05', 'ФПМИ', 'Б99-001', 8.1, 'Грант', 'Учится'),
       ('Дарина', 'Зайцева', 'Артемовна', '2003-03-30', 'ФПМИ', 'Б99-001', 8.0, 'Платная основа', 'Учится'),
       ('Дмитрий', 'Лаптев', 'Иванович', '2002-04-21', 'ФПМИ', 'Б99-002', 5.5, 'Бюджет', 'Учится'),
       ('Андрей', 'Райгородский', 'Михайлович', '1976-06-18', 'ФПМИ', 'Б99-002', 10.0, 'Бюджет', 'Учится'),
       ('Виктория', 'Матвеева', 'Ивановна', '2001-05-10', 'ФПМИ', 'Б99-002', 4.0, 'Бюджет', 'Отчислен'),
       ('Даниил', 'Маслов', 'Даниилович', '2003-03-08', 'ФПМИ', 'Б99-002', 6.7, 'Платная основа', 'Учится'),
       ('Вячеслав', 'Никитин', 'Александрович', '2004-11-12', 'ФПМИ', 'Б99-002', 9.3, 'Бюджет', 'Учится'),
       ('Виктор', 'Цветков', 'Ильич', '2001-01-13', 'ФПМИ', 'Б99-003', 5.0, 'Платная основа', 'В академе'),
       ('Алиса', 'Смирнова', 'Тимофеевна', '2002-12-31', 'ФПМИ', 'Б99-003', 7.5, 'Бюджет', 'Учится'),
       ('Давид', 'Иванов', 'Маркович', '2002-03-19', 'ФПМИ', 'Б99-003', 6.1, 'Бюджет', 'Учится'),
       ('Никита', 'Осипов', 'Ростиславович', '2002-08-15', 'ФПМИ', 'Б99-003', 6.3, 'Грант', 'Учится'),
       ('Максим', 'Кочергин', 'Львович', '2001-02-05', 'ФПМИ', 'Б99-003', 8.5, 'Бюджет', 'Учится');

insert into university_database.student_list (name, surname, patronymic, birth_date, faculty, group_name, gpa, fee_status, status)
values ('Артем', 'Беляков', 'Ильич', '2004-02-15', 'ФРКТ', 'Б98-001', 8.8, 'Бюджет', 'Учится'),
       ('Ева', 'Сергеева', 'Марковна', '2003-09-17', 'ФРКТ', 'Б98-001', 9.0, 'Бюджет', 'Учится'),
       ('Елизавета', 'Игнатова', 'Ивановна', '2005-11-21', 'ФРКТ', 'Б98-001', 7.1, 'Грант', 'Учится'),
       ('Мирон', 'Тарасов', 'Федорович', '2002-05-31', 'ФРКТ', 'Б98-002', 4.1, 'Бюджет', 'Учится'),
       ('Алексей', 'Соловьев', 'Миохайлович', '2001-04-11', 'ФРКТ', 'Б98-002', 5.6, 'Платная основа', 'В академе'),
       ('Даниил', 'Верещагин', 'Никитич', '2002-01-05', 'ФРКТ', 'Б98-002', 7.3, 'Бюджет', 'Учится'),
       ('Александра', 'Кравцова', 'Владимировна', '2003-03-02', 'ФРКТ', 'Б98-003', 3.9, 'Бюджет', 'Отчислен'),
       ('Степан', 'Морозов', 'Степанович', '2002-02-22', 'ФРКТ', 'Б98-003', 9.1, 'Платная основа', 'Учится'),
       ('Филипп', 'Волков', 'Родионович', '2004-08-23', 'ФРКТ', 'Б98-003', 6.7, 'Бюджет', 'Учится'),
       ('Виктория', 'Алексеева', 'Адамовна', '2006-10-28', 'ФРКТ', 'Б98-003', 5.3, 'Бюджет', 'Учится');

insert into university_database.student_list (name, surname, patronymic, birth_date, faculty, group_name, gpa, fee_status, status)
values ('Злата', 'Зуева', 'Владиславовна', '2005-01-25', 'ЛФИ', 'Б97-001', 6.9, 'Бюджет', 'Учится'),
       ('Екатерина', 'Иванова', 'Максимовна', '2004-03-15', 'ЛФИ', 'Б97-001', 9.1, 'Платная основа', 'Учится'),
       ('Анастасия', 'Никулина', 'Арсентьевна', '2006-06-24', 'ЛФИ', 'Б97-001', 3.9, 'Бюджет', 'Учится'),
       ('Алина', 'Филимонова', 'Матвеевна', '2004-01-21', 'ЛФИ', 'Б97-001', 6.0, 'Бюджет', 'В академе'),
       ('Валерия', 'Мельникова', 'Сергеевна', '2001-10-29', 'ЛФИ', 'Б97-002', 7.8, 'Бюджет', 'Учится'),
       ('Анатолий', 'Иванов', 'Сергеевич', '2004-06-04', 'ЛФИ', 'Б97-002', 6.8, 'Платная основа', 'Учится'),
       ('Даниил', 'Москвин', 'Степанович', '2002-08-21', 'ЛФИ', 'Б97-002', 8.6, 'Бюджет', 'Учится'),
       ('Елизавета', 'Лазарева', 'Макаровна', '2003-04-18', 'ЛФИ', 'Б97-002', 5.8, 'Бюджет', 'Учится'),
       ('Любовь', 'Соловьева', 'Руслановна', '2004-03-07', 'ЛФИ', 'Б97-003', 7.0, 'Бюджет', 'Учится'),
       ('Михаил', 'Высоцкий', 'Маркович', '2006-03-30', 'ЛФИ', 'Б97-003', 6.3, 'Грант', 'Учится'),
       ('Андрей', 'Филатов', 'Егорович', '2004-07-12', 'ЛФИ', 'Б97-003', 7.2, 'Бюджет', 'Учится'),
       ('Маслова', 'Александра', 'Дмитриевна', '2001-12-13', 'ЛФИ', 'Б97-003', 4.8, 'Бюджет', 'В академе'),
       ('Тимур', 'Зайцев', 'Константинович', '2003-03-05', 'ЛФИ', 'Б97-003', 6.7, 'Платная основа', 'Учится');

insert into university_database.student_list (name, surname, patronymic, birth_date, faculty, group_name, gpa, fee_status, status)
values ('Павел', 'Бондарев', 'Юрьевич', '2005-02-25', 'ФАКТ', 'Б96-001', 5.3, 'Бюджет', 'В академе'),
       ('Амина', 'Сазонова', 'Родионовна', '2003-01-21', 'ФАКТ', 'Б96-001', 7.9, 'Платная основа', 'Учится'),
       ('Ульяна', 'Маркова', 'Мирославовна', '2003-08-22', 'ФАКТ', 'Б96-001', 8.3, 'Бюджет', 'Учится'),
       ('Никита', 'Егоров', 'Тимурович', '2004-05-13', 'ФАКТ', 'Б96-001', 4.6, 'Грант', 'Учится'),
       ('Кира', 'Бирюкова', 'Сергеевна', '2002-08-31', 'ФАКТ', 'Б96-002', 7.2, 'Бюджет', 'В академе'),
       ('Максим', 'Малахов', 'Михайлович', '2001-10-26', 'ФАКТ', 'Б96-002', 9.5, 'Бюджет', 'Учится'),
       ('Марк', 'Рубцов', 'Леонович', '2005-04-30', 'ФАКТ', 'Б96-002', 9.3, 'Бюджет', 'Учится'),
       ('Анастасия', 'Морозова', 'Максимовна', '2006-11-21', 'ФАКТ', 'Б96-002', 5.9, 'Бюджет', 'Учится'),
       ('Георгий', 'Елисеев', 'Леонидович', '2005-02-28', 'ФАКТ', 'Б96-003', 8.1, 'Бюджет', 'Учится'),
       ('Алиса', 'Сидорова', 'Данииловна', '2004-03-22', 'ФАКТ', 'Б96-003', 4.9, 'Платная основа', 'Отчислен'),
       ('Варвара', 'Мельникова', 'Ильинична', '2005-09-15', 'ФАКТ', 'Б96-003', 5.3, 'Бюджет', 'Отчислен');

insert into university_database.student_list (name, surname, patronymic, birth_date, faculty, group_name, gpa, fee_status, status)
values ('Мирон', 'Масленников', 'Антонович', '2003-03-21', 'ВШПИ', 'Б01-001', 8.1, 'Бюджет', 'Учится'),
       ('Вероника', 'Жукова', 'Артемовна', '2002-09-12', 'ВШПИ', 'Б01-001', 8.3, 'Платная основа', 'Учится'),
       ('Мария', 'Рогова', 'Ильинична', '2005-05-31', 'ВШПИ', 'Б01-001', 7.5, 'Бюджет', 'Учится'),
       ('Иван', 'Куликов', 'Максимович', '2001-01-15', 'ВШПИ', 'Б01-001', 9.2, 'Бюджет', 'Учится'),
       ('Кирилл', 'Исаев', 'Никитич', '2005-08-05', 'ВШПИ', 'Б01-002', 9.3, 'Грант', 'Учится'),
       ('Алексей', 'Малеев', 'Викторович', '1988-08-11', 'ВШПИ', 'Б01-002', 10.0, 'Бюджет', 'Учится'),
       ('Елизавета', 'Орлова', 'Максимовна', '2004-05-31', 'ВШПИ', 'Б01-002', 8.5, 'Платная основа', 'Учится'),
       ('Артем', 'Копылов', 'Артемович', '2006-02-21', 'ВШПИ', 'Б01-002', 7.5, 'Бюджет', 'Учится'),
       ('Матвей', 'Царев', 'Иванович', '2002-08-14', 'ВШПИ', 'Б01-003', 8.1, 'Платная основа', 'Учится'),
       ('Константин', 'Фролов', 'Саввич', '2003-06-21', 'ВШПИ', 'Б01-003', 7.3, 'Бюджет', 'Учится'),
       ('Варвара', 'Белкина', 'Адамовна', '2005-07-16', 'ВШПИ', 'Б01-003', 7.6, 'Бюджет', 'Учится'),
       ('Полина', 'Александрова', 'Федоровна', '2006-04-18', 'ВШПИ', 'Б01-003', 8.4, 'Грант', 'Учится');

insert into university_database.student_list (name, surname, patronymic, birth_date, faculty, group_name, gpa, fee_status, status)
values ('Кирилл', 'Титов', 'Матвеевич', '2003-03-21', 'ФБМФ', 'Б95-001', 5.5, 'Бюджет', 'Учится'),
       ('Ксения', 'Смирнова', 'Романовна', '2003-03-21', 'ФБМФ', 'Б95-001', 7.1, 'Платная основа', 'Учится'),
       ('Екатерина', 'Иванова', 'Платоновна', '2003-03-21', 'ФБМФ', 'Б95-001', 3.9, 'Бюджет', 'В академе'),
       ('Анастасия', 'Петровская', 'Матвеевна', '2003-03-21', 'ФБМФ', 'Б95-002', 6.6, 'Бюджет', 'Учится'),
       ('Михаил', 'Зотов', 'Тимофеевич', '2003-03-21', 'ФБМФ', 'Б95-002', 9.1, 'Грант', 'Учится'),
       ('Макар', 'Степанов', 'Даниилович', '2003-03-21', 'ФБМФ', 'Б95-002', 8.5, 'Бюджет', 'Учится'),
       ('Марк', 'Платонов', 'Матвеевич', '2003-03-21', 'ФБМФ', 'Б95-003', 7.7, 'Бюджет', 'Отчислен'),
       ('Варвара', 'Кузнецова', 'Максимовна', '2003-03-21', 'ФБМФ', 'Б95-003', 7.1, 'Платная основа', 'Учится'),
       ('Мирон', 'Виноградов', 'Михайлович', '2003-03-21', 'ФБМФ', 'Б95-003', 9.1, 'Бюджет', 'Учится'),
       ('Ева', 'Жданова', 'Данииловна', '2003-03-21', 'ФБМФ', 'Б95-003', 6.2, 'Бюджет', 'Учится');




-- Заполнение оставшихся табличек на основе информации из student_list

update university_database.faculties
set num_of_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФПМИ' and fee_status = 'Бюджет'
    ),
    num_of_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФПМИ' and (fee_status = 'Платная основа' or fee_status = 'Грант')
    ),
    num_of_free_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФПМИ' and fee_status = 'Бюджет' and (status = 'В академе' or status = 'Отчислен')
    ),
    num_of_free_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФПМИ' and (fee_status = 'Платная основа' or fee_status = 'Грант') and (status = 'В академе' or status = 'Отчислен')
    )
where faculty = 'ФПМИ';

update university_database.faculties
set num_of_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФРКТ' and fee_status = 'Бюджет'
    ),
    num_of_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФРКТ' and (fee_status = 'Платная основа' or fee_status = 'Грант')
    ),
    num_of_free_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФРКТ' and fee_status = 'Бюджет' and (status = 'В академе' or status = 'Отчислен')
    ),
    num_of_free_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФРКТ' and (fee_status = 'Платная основа' or fee_status = 'Грант') and (status = 'В академе' or status = 'Отчислен')
    )
where faculty = 'ФРКТ';

update university_database.faculties
set num_of_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ЛФИ' and fee_status = 'Бюджет'
    ),
    num_of_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ЛФИ' and (fee_status = 'Платная основа' or fee_status = 'Грант')
    ),
    num_of_free_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ЛФИ' and fee_status = 'Бюджет' and (status = 'В академе' or status = 'Отчислен')
    ),
    num_of_free_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ЛФИ' and (fee_status = 'Платная основа' or fee_status = 'Грант') and (status = 'В академе' or status = 'Отчислен')
    )
where faculty = 'ЛФИ';

update university_database.faculties
set num_of_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФАКТ' and fee_status = 'Бюджет'
    ),
    num_of_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФАКТ' and (fee_status = 'Платная основа' or fee_status = 'Грант')
    ),
    num_of_free_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФАКТ' and fee_status = 'Бюджет' and (status = 'В академе' or status = 'Отчислен')
    ),
    num_of_free_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФАКТ' and (fee_status = 'Платная основа' or fee_status = 'Грант') and (status = 'В академе' or status = 'Отчислен')
    )
where faculty = 'ФАКТ';

update university_database.faculties
set num_of_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ВШПИ' and fee_status = 'Бюджет'
    ),
    num_of_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ВШПИ' and (fee_status = 'Платная основа' or fee_status = 'Грант')
    ),
    num_of_free_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ВШПИ' and fee_status = 'Бюджет' and (status = 'В академе' or status = 'Отчислен')
    ),
    num_of_free_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ВШПИ' and (fee_status = 'Платная основа' or fee_status = 'Грант') and (status = 'В академе' or status = 'Отчислен')
    )
where faculty = 'ВШПИ';

update university_database.faculties
set num_of_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФБМФ' and fee_status = 'Бюджет'
    ),
    num_of_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФБМФ' and (fee_status = 'Платная основа' or fee_status = 'Грант')
    ),
    num_of_free_budget_funded_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФБМФ' and fee_status = 'Бюджет' and (status = 'В академе' or status = 'Отчислен')
    ),
    num_of_free_non_budget_seats = (
        select count(*)
        from university_database.student_list
        where faculty = 'ФБМФ' and (fee_status = 'Платная основа' or fee_status = 'Грант') and (status = 'В академе' or status = 'Отчислен')
    )
where faculty = 'ФБМФ';

update university_database.fee_info
set number_of_students = (
        select count(*)
        from university_database.student_list
        where fee_status = 'Бюджет' and status = 'Учится'
    )
where fee_status = 'Бюджет';

update university_database.fee_info
set number_of_students = (
        select count(*)
        from university_database.student_list
        where fee_status = 'Платная основа' and status = 'Учится'
    )
where fee_status = 'Платная основа';

update university_database.fee_info
set number_of_students = (
        select count(*)
        from university_database.student_list
        where fee_status = 'Грант' and status = 'Учится'
    )
where fee_status = 'Грант';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б99-001' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б99-001' and status = 'Учится'
    )
where group_name = 'Б99-001';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б99-002' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б99-002' and status = 'Учится'
    )
where group_name = 'Б99-002';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б99-003' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б99-003' and status = 'Учится'
    )
where group_name = 'Б99-003';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б98-001' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б98-001' and status = 'Учится'
    )
where group_name = 'Б98-001';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б98-002' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б98-002' and status = 'Учится'
    )
where group_name = 'Б98-002';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б98-003' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б98-003' and status = 'Учится'
    )
where group_name = 'Б98-003';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б97-001' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б97-001' and status = 'Учится'
    )
where group_name = 'Б97-001';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б97-002' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б97-002' and status = 'Учится'
    )
where group_name = 'Б97-002';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б97-003' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б97-003' and status = 'Учится'
    )
where group_name = 'Б97-003';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б96-001' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б96-001' and status = 'Учится'
    )
where group_name = 'Б96-001';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б96-002' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б96-002' and status = 'Учится'
    )
where group_name = 'Б96-002';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б96-003' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б96-003' and status = 'Учится'
    )
where group_name = 'Б96-003';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б01-001' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б01-001' and status = 'Учится'
    )
where group_name = 'Б01-001';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б01-002' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б01-002' and status = 'Учится'
    )
where group_name = 'Б01-002';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б01-003' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б01-003' and status = 'Учится'
    )
where group_name = 'Б01-003';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б95-001' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б95-001' and status = 'Учится'
    )
where group_name = 'Б95-001';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б95-002' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б95-002' and status = 'Учится'
    )
where group_name = 'Б95-002';

update university_database.group
set number_of_students = (
        select count(*)
        from university_database.student_list
        where group_name = 'Б95-003' and status = 'Учится'
    ),
    group_gpa = (
        select round(cast(sum(gpa) as NUMERIC) / count(*), 1)
        from university_database.student_list
        where group_name = 'Б95-003' and status = 'Учится'
    )
where group_name = 'Б95-003';