create view HSSE_status_information as
select name, surname, patronymic, group_name, fee_status, status
from university_database.student_list
where faculty = 'ВШПИ'
with local check option;

create view personal_information as
select student_id, faculty, group_name, gpa, fee_status, status
from university_database.student_list;

create view faculty_avg_gpa as
select t1.faculty, round(cast(avg(t2.gpa) as numeric), 1) as average_gpa
from university_database.faculties t1
join university_database.student_list t2 on t1.faculty = t2.faculty
group by t1.faculty;

create view gpa_rank as
select dense_rank() over w as rank, name, surname, group_name, gpa
from university_database.student_list
window w as (
    partition by group_name
    order by gpa desc
);