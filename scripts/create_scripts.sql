create schema university_database;

create table university_database.faculties (
    faculty text primary key,
    num_of_budget_funded_seats integer,
    num_of_non_budget_seats integer,
    num_of_free_budget_funded_seats integer,
    num_of_free_non_budget_seats integer
);

create table university_database.group (
    group_name text primary key,
    number_of_students integer,
    group_gpa float
);

create table university_database.fee_info (
    fee_status text primary key,
    number_of_students integer
);

create table university_database.student_list (
    student_id serial primary key,
    name text not null,
    surname text not null,
    patronymic text,
    birth_date date not null,
    faculty text not null,
    group_name text not null,
    gpa float,
    fee_status text not null,
    status text not null,
    foreign key (faculty)
    references university_database.faculties (faculty),
    foreign key (group_name)
    references university_database.group (group_name),
    foreign key (fee_status)
    references university_database.fee_info (fee_status)
);