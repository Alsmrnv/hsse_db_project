--Процедура для добавления нового студента в базу данных
create procedure add_student(
  in_name text,
  in_surname text,
  in_patronymic text,
  in_birth_date date,
  in_faculty text,
  in_group_name text,
  in_gpa float,
  in_fee_status text,
  in_status text
)
language SQL
as $$
    insert into university_database.student_list (name, surname, patronymic, birth_date, faculty, group_name, gpa, fee_status, status)
    values (in_name, in_surname, in_patronymic, in_birth_date, in_faculty, in_group_name, in_gpa, in_fee_status, in_status)
$$;


--Триггер для автоматического обновления числа студентов в группе при добавлении нового студента
create function update_group_number() returns trigger
as $$
  begin
    update university_database.group
      set number_of_students = number_of_students + 1
    where group_name = NEW.group_name;
  end;
$$ language plpgsql;

create trigger update_group_number_trigger
    after insert on university_database.student_list
    for each row
    execute function update_group_number();

--Триггер для автоматического обновления числа студентов в табличке fee_info
create function update_fee_info() returns trigger
as $$
  begin
    update university_database.fee_info
      set number_of_students = number_of_students + 1
    where fee_status = NEW.fee_status;
  end;
$$ language plpgsql;

create trigger update_fee_info_trigger
    after insert on university_database.student_list
    for each row
    execute function update_fee_info();

--Процедура для удаления студента из базы данных по его ID
create procedure delete_student(in_student_id integer)
as $$
  begin
    delete from university_database.student_list
    where student_id = in_student_id;
  end;
$$ language plpgsql;